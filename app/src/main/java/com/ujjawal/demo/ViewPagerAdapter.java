package com.ujjawal.demo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;

import java.util.Objects;

public class ViewPagerAdapter extends PagerAdapter {

    // Context object
    Context context;


    // Layout Inflater
    LayoutInflater mLayoutInflater;


    // Viewpager Constructor
    public ViewPagerAdapter(Context context) {
        this.context = context;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // return the number of images
        return AppGlobal.dataSetArrayList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == ((LinearLayout) object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        // inflating the item.xml
        View itemView = mLayoutInflater.inflate(R.layout.imagedetailslistitem, container, false);

        // referencing the image view from the item.xml file
        ImageView imageView = (ImageView) itemView.findViewById(R.id.image);
        TextView metadata = (TextView) itemView.findViewById(R.id.meta_data);

        // setting the image in the imageView
        UrlImageViewHelper.setUrlDrawable(imageView, AppGlobal.dataSetArrayList.get(position).getUrl());

        metadata.setText("Title : "+AppGlobal.dataSetArrayList.get(position).getTitle()+"\n"
                +"Explanation : "+AppGlobal.dataSetArrayList.get(position).getExplanation()+"\n"

                +"copyright : "+AppGlobal.dataSetArrayList.get(position).getCopyrightString()+"\n"
                +"date : "+AppGlobal.dataSetArrayList.get(position).getDate()+"\n"
                +"service_version : "+AppGlobal.dataSetArrayList.get(position).getService_version()+"\n"
                +"hdurl : "+AppGlobal.dataSetArrayList.get(position).getHdurl()+"\n");
        // Adding the View
        Objects.requireNonNull(container).addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        container.removeView((LinearLayout) object);
    }
}
