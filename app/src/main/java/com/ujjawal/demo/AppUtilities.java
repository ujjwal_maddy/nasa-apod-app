package com.ujjawal.demo;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class AppUtilities {


    static String getJsonFromAssets(Context context) {
        String jsonString;
        try {
            InputStream is = context.getAssets().open("data.json");

            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();

            jsonString = new String(buffer, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        return jsonString;
    }

    public static  void ParseData(Context context){

        try {
            JSONArray mainArray = new JSONArray(getJsonFromAssets(context));

            AppGlobal.dataSetArrayList  = new ArrayList<>();

            for (int i = 0; i < mainArray.length(); i++) {
                JSONObject obj = mainArray.getJSONObject(i);
                AppGlobal.dataSetArrayList.add(new DataSet(obj.getString("copyright"),
                        obj.getString("date"),
                        obj.getString("explanation"),
                        obj.getString("hdurl"),
                        obj.getString("media_type"),
                        obj.getString("service_version"),
                        obj.getString("title"),
                        obj.getString("url")));
            }




        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
