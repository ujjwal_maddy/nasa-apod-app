package com.ujjawal.demo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity implements ListItemClick{

    RecyclerView grid_image ;

    GridImageAdap gridImageAdap;

    ListItemClick listItemClick;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listItemClick = this;

        grid_image = (RecyclerView)findViewById(R.id.grid_image);

        AppUtilities.ParseData(MainActivity.this);

        gridImageAdap = new GridImageAdap(AppGlobal.dataSetArrayList,  this ,listItemClick);

        LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        grid_image.setLayoutManager(horizontalLayoutManager);
        grid_image.setAdapter(gridImageAdap);



    }


    @Override
    public void getEvent(String tag, int position) {
        startActivity(new Intent(MainActivity.this, DetailsScreen.class).putExtra("pos",position));
    }


}