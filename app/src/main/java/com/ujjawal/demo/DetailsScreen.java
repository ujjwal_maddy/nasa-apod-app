package com.ujjawal.demo;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

public class DetailsScreen extends AppCompatActivity {


    ViewPager viewPagerimage ;

    ViewPagerAdapter mViewPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_detailscreen);

        // Initializing the ViewPager Object
        viewPagerimage = (ViewPager)findViewById(R.id.viewPagerimage);

        // Initializing the ViewPagerAdapter
        mViewPagerAdapter = new ViewPagerAdapter(DetailsScreen.this);

        // Adding the Adapter to the ViewPager
        viewPagerimage.setAdapter(mViewPagerAdapter);

        viewPagerimage.setCurrentItem(getIntent().getExtras().getInt("pos"));

    }


}
