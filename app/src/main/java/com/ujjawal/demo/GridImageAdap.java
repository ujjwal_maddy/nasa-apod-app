package com.ujjawal.demo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.recyclerview.widget.RecyclerView;

import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;

import java.util.ArrayList;

public class GridImageAdap extends RecyclerView.Adapter<GridImageAdap.MyViewHolder>  {


    ArrayList<DataSet> dataList = new ArrayList<>();
    Context context;

    ListItemClick listItemClick;

    public GridImageAdap(ArrayList<DataSet> horizontalList, Context context , ListItemClick listItemClick) {
        this.dataList = horizontalList;
        this.context = context;
        this.listItemClick = listItemClick;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView image ;


        public MyViewHolder(View view) {
            super(view);
            image =(ImageView) view.findViewById(R.id.gridimage);


        }
    }



    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.imagegriditem, parent, false);
        itemView.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {


        UrlImageViewHelper.setUrlDrawable(holder.image, dataList.get(position).getUrl());

        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listItemClick.getEvent("change", position);
            }
        });



    }


    @Override
    public int getItemCount()
    {
        return dataList.size();
    }
}
